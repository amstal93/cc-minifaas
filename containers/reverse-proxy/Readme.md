# Nginx and Docker

![](https://miro.medium.com/max/3000/1*5H07sEFDVqS9HuNGvrstXg.png)



NGINX is an open-source web server that also serves as a reverse proxy and HTTP load balancer.

**NGINX is a reverse proxy**
A reverse proxy is a server that sits in front of a group of web servers. When a browser makes an HTTP request, the request first goes to the reverse proxy, which then sends the request to the appropriate web server.

**NGINX is a HTTP load balancer**
A load balancer is responsible for routing client HTTP requests to web servers in an efficient manner. This prevents any individual web server from being overworked.
A load balancer can also be configured so that if a web server goes down, the reverse proxy will no longer forward requests to that web server