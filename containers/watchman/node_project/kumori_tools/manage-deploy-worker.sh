#!/bin/bash
take_decision=$1
type=$2
number_adds=$3
if [[ -z $take_decision ]]; then
  echo "Error not decision"
  exit 127
fi
if [[ -z $number_adds ]]; then
  number_adds=1
fi
function decision(){
	if [[ $take_decision == *"create"* ]]; then
   if [ "$(($1 + $number_adds))" -lt "6" ]; then
      numInstance=$(($1 + $number_adds))
    fi
	fi
	if [[ $take_decision == *"kill"* ]]; then
    if [ "$1" -gt "1" ]; then
      numInstance=$(($1 - 1))
    fi
	fi
  if [ "$numInstance" != '' ];then
	  sed -i "s/^.*role:\s$type:.*$/description: role: $type: rsize: \$_instances: ${numInstance}/" ./kumori_tools/cue-manifests/dinamic_deployment/mainfest.cue
    echo $numInstance
  fi
  
}
if [[ -z $type ]]; then
   echo "Error not type specificed"
  exit 127
fi
num_instances=""
num_instances=$(cat ./kumori_tools/cue-manifests/dinamic_deployment/mainfest.cue | grep "role: $type" | sed 's/.*\(.\)/\1/')
if [[ -z $num_instances ]]; then
  echo "Error not foud $type"
  exit 127
fi
decision $num_instances
