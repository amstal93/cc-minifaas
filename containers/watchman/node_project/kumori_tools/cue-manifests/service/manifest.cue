package faas_service

import (
  k         "kumori.systems/kumori/kmv"
  frontend  "kumori.systems/minifaas/frontend"
  nats      "kumori.systems/minifaas/nats"
  db      "kumori.systems/minifaas/db"
  worker      "kumori.systems/minifaas/worker"
  watchman      "kumori.systems/minifaas/watchman"
)

let mefrontend = frontend.#Manifest
let menats = nats.#Manifest
let medb = db.#Manifest
let meworker = worker.#Manifest
let mewatchman = watchman.#Manifest
#Manifest: k.#ServiceManifest & {

  ref: {
    domain: "kumori.systems.minifaas"
    name: "faas_service"
    version: [0,0,1]
  }

  description: {
    srv: {
      server: {
        service: {
          protocol: "http"
          port: 80
        }
      }
    }

    config: {
      parameter: {
        frontend  : mefrontend.description.config.parameter
        nats	  : menats.description.config.parameter
        db	  : medb.description.config.parameter
        worker	  : meworker.description.config.parameter
        watchman : mewatchman.description.config.parameter
      }
      resource: mefrontend.description.config.resource
    }

    // Config spread
    role: {

      nats: k.#Role
      nats: artifact: menats
      nats: cfg: parameter: config.parameter.nats

      db: k.#Role
      db: artifact: medb
      db: cfg: parameter: config.parameter.db

      frontend: k.#Role
      frontend: artifact: mefrontend
      frontend: cfg: parameter: config.parameter.frontend

      worker: k.#Role
      worker: artifact: meworker
      worker: cfg: parameter: config.parameter.worker

      watchman: k.#Role
      watchman: artifact: mewatchman
      watchman: cfg: parameter: config.parameter.watchman

    }

    connector: {
      serviceconnector: {kind: "lb"}
      lbconnector:      {kind: "lb"}
      lbconnector2:      {kind: "lb"}
      lbconnector3:      {kind: "lb"}
      lbconnector4:      {kind: "lb"}
      fullconnector:    {kind: "full"}
      fullconnector2:    {kind: "full"}
      fullconnector3:    {kind: "full"}
    }

    link: {

      // Outside -> FrontEnd (LB connector)
			self: service: to: "serviceconnector"
      serviceconnector: to: frontend: "entrypoint"
     
       //Frontend -> Nats
      frontend: natsclient: to: "lbconnector"
      lbconnector: to: nats: "natserver"
       //Frontend -> db
      frontend: dbclient: to: "lbconnector2"
      lbconnector2: to: db: "dbserver"
      //Cluster HA DB
      //db: maindbcluster: to: "fullconnector"
      fullconnector: to: db: "maindbcluster"
      
      //Cluster HA NATS
      fullconnector2: to: nats: "natscluster"
      fullconnector3: to: nats: "natscluserver"
      // Worker -> Nats
      worker: natsclient: to: "lbconnector3"
      lbconnector3: to: nats: "natserver"
      // Watchman -> Nats
      watchman: natsclient: to: "lbconnector4"
      lbconnector4: to: nats: "natserver"

       //Worker to fullconnector
      //  worker: natsclient: to: "fullconnector"

   }
  }
}
