package frontend

import k "kumori.systems/kumori/kmv"

#Manifest:  k.#ComponentManifest & {

  ref: {
    domain: "kumori.systems.minifaas"
    name: "faas_frontend"
    version: [0,0,1]
  }

  description: {

    srv: {
      server: entrypoint: {
        protocol: "http"
        port:     8080
      }
      client: natsclient: {
        protocol: "tcp"
      }
      client: dbclient: {
       protocol: "tcp"
      }
    }

    config: {
      resource: {}
      parameter: {
       config: {
          param_one : string | *"default_param_one"
          param_two : number | *"default_param_two"
        }
        urlnats: "0.natsclient"
        restapiclientPortEnv: string | *"80"
      }
    }

    size: {
      $_memory: *"100Mi" | uint
      $_cpu: *"100m" | uint
    }

    code: frontend: k.#Container & {
      name: "frontend"
      image: {
        hub: {
          name: "registry.hub.docker.com"
          secret: ""
        }
        tag: "mamarbao/faas-frontend:9.1"
      }
      mapping: {
        filesystem: [
          {
            path: "/config/config.json"
            data: config.parameter.config
            format: "json"
          },
        ]
        env: {
          DB_HOST: "0.dbclient",
          DB_PORT: "80",
          URL_NAT: "0.natsclient",
          PORT_NAT: "80",
          PORT: "8080",
          RESTAPICLIENT_PORT_ENV: config.parameter.restapiclientPortEnv
          SERVER_PORT_ENV: "\(srv.server.entrypoint.port)"
        }
      }
    }
  }
}
