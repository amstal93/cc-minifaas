function execShellCommandTimeout(cmd,_delay=5000) {
    const exec = require('child_process').exec;
    return  Promise.race([new Promise((resolve, reject) => {
       exec(cmd, (error, stdout, stderr) => {
          if (error){reject(error); console.log({error})};
          resolve(stdout ? stdout : stderr);
       });
    }),new Promise((_, reject) => setTimeout(() => reject(new Error('timeout')), _delay))
  
  ]);
}

async function take_decision(decision,type,instances=1,_delay=10000) {
   // PWD when call this function = where this function is called ...../node_project (called with watchman_agent)
    try{
      //manage-deploy-worker.sh
      if(!decision || !type) return {message: "Inssuficient params", success: false}
       result = await execShellCommandTimeout(`./kumori_tools/manage-deploy-worker.sh ${decision} ${type} "${instances}"`,_delay);
       
    }catch(error) {
       console.warn("error",error.message)
       let {message} = error;
       return {message: "failed to update instances on cue desployment", success: false}
    }
    console.log("[localCommand take_decision successfully]",{result});
    
    return {message: result, success: true};
  }

  async function curret_instances(type,_delay=5000) {
   // PWD when call this function = where this function is called ...../node_project (called with watchman_agent)
    try{
      //manage-deploy-worker.sh
      if(!type) return {message: "Inssuficient params", success: false}
       result = await execShellCommandTimeout(`cat ./kumori_tools/cue-manifests/dinamic_deployment/mainfest.cue | grep "role: ${type}"`,_delay);
       
    }catch(error) {
       console.warn("error",error.message)
       let {message} = error;
       return {message: "No get info about current deployment", success: false}
    }
    
    return {message: result, success: true};

  }

  async function set_decision(_delay=110000) {
    // PWD when call this function = where this function is called ...../node_project (called with watchman_agent)
     try{
        console.log(`Wait set_decision until ${_delay}` )
        result = await 
        execShellCommandTimeout(`./kumori_tools/kumori_instance.sh`,_delay);
      
     }catch(error) {
        console.warn("error",error.message)
        let {message} = error;
        return {message: "failed to update desployment kumori", success: false}
     }
     console.log("[localCommand set_decision successfully]",{result});
     return {message: result, success: true};
   }
  exports.take_decision = take_decision;
  exports.set_decision = set_decision;
  exports.curret_instances = curret_instances;