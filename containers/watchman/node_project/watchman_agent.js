const NATS = require('nats');
const { v1: uuidv1 } = require('uuid'); // Generate a v1 (time-based) id

const exec = require('./kumori_tools/commands');
const WatchMan = require("./tools/watchman-stats")

const URL_NAT = process.env.URL_NAT || '0.0.0.0';
const PORT_NAT = process.env.PORT_NAT || '4222';
NATS_URL = `nats://${URL_NAT}:${PORT_NAT}`;
var nats;

var Lower_Threshold = process.env.Lower_Threshold || '1000';
var Upper_Threshold = process.env.Upper_Threshold || '8000';

var myoperations=[]
const { validate } = require('jsonschema');
const decisionSchema = require('./tools/schema/decisionSchema.json');


//Instance Watchman-stats-queue
var mWatchMan = WatchMan.instance()

function connectNATS(maxRetries = 3, retryInterval = 5000) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      nats = NATS.connect({ url: NATS_URL, maxPingOut: 10, maxReconnectAttempts: -1, reconnectTimeWait: 250 });
      nats.on('error', (err) => {
        console.log(`[ERROR][RETRY:${maxRetries}] ${err}`)
        if (maxRetries == 0) {
          if (!nats.connected)
            reject('Error when trying to connecte to nats');
        } else resolve(connectNATS(maxRetries - 1, retryInterval));

      })

      //once connected, if disconnected it will attempt to reconnect.
      nats.on('reconnecting', () => {
        console.log('[INFO] reconnecting')
      })

      nats.on("connect", () => {
        console.log('[SUCCESS] NATS Worker connected')
        resolve(nats)
      });
    }, retryInterval);
  });
}
function disconnect() {
  if (nats) {
    nats.flush(() => {
      nats.close();
      nats = null;
    });
  }
}

function isObject(obj)
{
    return obj !== undefined && obj !== null && obj.constructor == Object;
}

function taking_a_decision() {
  let mAVG = mWatchMan.getAvarageTime()
  console.log(`\n====================TAKE A DECISION=================`)
  console.log(`1. AVG durantion (${mAVG}) >= [${Upper_Threshold}] then kill a worker`)
  console.log(`2. AVG durantion (${mAVG}) <= ${Lower_Threshold} then create a worker\n`)
  //1 DECISION
  //  if(mAVG >= Upper_Threshold ) exec.take_decision("kill","worker",1)
  //2 DECISION
  // if(mAVG <= Lower_Threshold ) exec.take_decision("create","worker",1)
  console.log(myoperations)
}
// need to write a service that sends a request to the server every 5 seconds asking for data, but in case the server is overloaded, it should increase the interval to 10, 20, 40 seconds…
function check_stats_timer() {
  return setTimeout(function tick() {
    console.log(`[WM] Stats:`);
    mWatchMan.showStats();
    taking_a_decision();
    timerId = setTimeout(tick, 550000); // (*)
  }, 550000);
}

function init_subs() {
  if (!nats.connected) return false;
  nats.subscribe('stats-clients', {}, (msg, replyTo) => {
    mWatchMan.saveStats(JSON.parse(msg))
    console.log(`[WM] Get the msg: (${msg})`)
  })
  

  nats.subscribe('stats-frame', { queue: "stats.clients" }, (msg, replyTo) => {
    stats_frame = mWatchMan.getStats();
    nats.publish(replyTo, JSON.stringify(stats_frame))
  })

  async function apply_update_kumori(uuid,changes){
    let current_changes = JSON.parse(JSON.stringify(changes));
    let muuid=uuid;
    let={};
    try{
     result = await exec.set_decision(155000);
     result.changes=current_changes;
    }catch(e){
      result.error=`Update problems: type: ${m.type} instances: ${current_changes.instances} `
    }
    myoperations[muuid]=result
  }

  nats.subscribe('stats-take-decision-result', {}, (msg, replyTo) => {
    msg=JSON.parse(msg)
    try{
      let result_operation=myoperations[msg.uuid]
      nats.publish(replyTo, JSON.stringify(result_operation))
    }catch(err){
        console.log(err)
        nats.publish(JSON.stringify({succes:false,message:`Invalid format or invalid uuid ${msg.uuid}, wait some time!!`}))
    }
   
  })

  nats.subscribe('stats-take-decision', { queue: "stats.clients" },async (msg, replyTo) => {
    let m;
    let result={},changes={};
    let uuid ;
    try{
       m= isObject(msg)?msg:JSON.parse(msg)
       let check = validate(m, decisionSchema);
       if (!check.valid){
           console.error('[WM] Invalid JSON Format', check.errors.map(error => error.message))
           nats.publish(replyTo, JSON.stringify({success:false,error:check.errors.map(error => error.message),message:"Not valid params"}))
           return
        }
        try {
          let current = await exec.curret_instances(m.type)
          if(m.decision=="create"){
            
            changes =await exec.take_decision("create",m.type,m.instances)
           
          }else if(m.decision=="kill"){
            changes=await exec.take_decision("kill",m.type,1)
          }
         changes.current_instances=current.message
         changes.instances=changes.message;
         changes.type=m.type;
         result.success=true
         uuid = uuidv1();
         apply_update_kumori(uuid,changes)
        }catch(e){
          result.error=`Some problem when try ${m.decision} operation to ${m.type}`
        }
      
      }catch(err){
        console.log(err)
        result.error="Maybe not valid message"
    }
    if(result.error || result.success==false) nats.publish(replyTo, JSON.stringify({success:false,error:result.error,message:result.message}))
    else nats.publish(replyTo, JSON.stringify({uuid_operation:uuid,success:true,message:`Successful operation: ${m.decision}. It may take some time to establish itself, wait`,current_instances:changes.current_instances, updated_to_instances: changes.instances, type: changes.type }))
  })
  return true
}

//Send a request to a worker to finish
function requestKill(timout = 20000) {
  return new Promise((resolve, reject) => {
    if (!nats) reject('Necessary firts connection with Nats')
    if (!nats.connected) reject('Not connected to NATS Server')
    //just to receive a response message { max: 1 } and after close request
    let ssid = nats.request('kill-worker', 'kill worker', { max: 1 }, function (response) {
      console.log(`received = ${response}`)
      resolve(response)
    })
    nats.timeout(ssid, timout, 1, function (ssid) {
      console.log('[FAIL] request ssid ' + ssid + ' timed out, maybe worker instances = 0');
      reject('Function timed out')
    });
  });
}

console.log('[WM] Started WatchMan')
console.log(`[WM] Connecting to ${NATS_URL}`)
let timer;

connectNATS().then(value => {
  init_subs()
  timer = check_stats_timer();
}, reason => {
  console.error(`[WM] ${reason}`);
  if (timer)
    clearTimeout(timer);
});
