const NATS = require('nats');
//KUmori: URL_NAT: 0.natsclient (canal client)
const URL_NAT = process.env.URL_NAT || '0.0.0.0';
const PORT_NAT = process.env.PORT_NAT || '4222';
var NATS_URL = `nats://${URL_NAT}:${PORT_NAT}`;

class ProxyNats {

  constructor(mWatchMan, timeout = 10000) {
    this.ConnectionClient = null
    this.timeout = timeout
    this.url = NATS_URL;
    this.nats = null;
    this.mWatchMan=mWatchMan;
  }

  connectNATS(maxRetries = 3, retryInterval = 5000) {
    var _self = this;
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        _self.nats = NATS.connect({ url: NATS_URL, maxReconnectAttempts: 10, reconnectTimeWait: 350, verbose: true });
        _self.nats.on('error', (err) => {
          console.log(`[ERROR][RETRY:${maxRetries}] ${err}`)
          if (maxRetries == 0) {
            if (!_self.nats.connected)
              reject('Error when trying to connecte to nats');
          } else resolve(_self.connectNATS(maxRetries - 1, retryInterval));
        })

        _self.nats.on('close', function () {
          console.log('close')
        })
        // emitted whenever the client is attempting to reconnect
        _self.nats.on('reconnecting', () => {
          console.log('[INFO] reconnecting')
        })

        _self.nats.on("connect", () => {
          console.log('[SUCCESS] NATS Frontend connected')
          _self.init_subs();
          resolve(_self.nats)
        });
      }, retryInterval);
    });
  }


   init_subs() {
    var _self = this;
    if (!_self.nats.connected) return false;
  
    _self.nats.subscribe('stats-clients', {}, (msg, replyTo) => {
    _self.mWatchMan.saveStats(JSON.parse(msg))
     console.log(`[WM] Get the msg: (${msg})`)
    })
  
    return true
  }

  requestKill( timout = 20000) {
    var _self = this;
    return new Promise((resolve, reject) => {
      if (!_self.nats) reject('Necessary firts connection with Nats')
      if (!_self.nats.connected) reject('Not connected to NATS Server')
      //just to receive a response message { max: 1 } and after close request
      let ssid = _self.nats.request('kill-worker', 'kill worker', { max: 1 }, function (response) {
        console.log(`received = ${response}`)
        resolve(response)
      })
      _self.nats.timeout(ssid, timout, 1, function (ssid) {
        console.log('ssid ' + ssid + ' timed out');
        reject('Function timed out')
      });
    });
  }
  
}


class NatsSingleton {
  constructor() {
    throw new Error('Use Singleton.getInstance()');
  }
  static getInstance(mWatchMan, timeout = 8000) {
    if (!NatsSingleton.instance) {
      NatsSingleton.instance = new ProxyNats(mWatchMan,timeout);
    }
    return NatsSingleton.instance;
  }
}

module.exports.instance = (mWatchMan,timeout = 8000) => {
  return NatsSingleton.getInstance(mWatchMan, timeout = 8000);
}