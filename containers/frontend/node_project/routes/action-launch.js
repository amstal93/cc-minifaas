
const { validate } = require('jsonschema');
  const take_decisionSchema = require('./schema/decisionSchema.json');
module.exports.load = function (serverExpress,
  CockroachDB,
  ProxyNats) {
  /**
   * Por ahora funcion obtenida por el nombre // DEBE SER UNA FUNCION DE UN USUARIO REGISTRADO
   */
  serverExpress.get('/launch', async (req, res, next) => {
    let name_function = req.query.name_function
    let username = req.query.username
    let value_params = req.query.params_value
    console.log({value_params})
    if (!name_function) return next("It's necessary to specify a name_function")
    if (!username) return next("It's necessary to specify a username")
    let queryFunc;
    let queryUserID;

    //2. Get User on DB
    try {
      queryUserID = await CockroachDB.getUserID(username)
      if (queryUserID.message.length == 0) return next(`User not found: ${username}`);
      else user_id = queryUserID.message[0].user_id
    } catch (error) {
      return next(`Error trying to get user id from : ${username}`);
    }

    //2. Get Function on DB
    try {
      queryFunc = await CockroachDB.getFuncByName(name_function)
      if (queryFunc.message.length == 0) return next(`Not Exist Function name: ${name_function}`);
    } catch (error) {
      return next(`Error Trying to get functions: ${name_function}`);
    }
    let json_func = queryFunc.message[0]
    let { type, function_id, code } = json_func;
    let result, response;
    //2. Launch Function via NATS
    try {
      
      if (type == 'maths'){ 
        if(json_func.params){// Have a params
          if(!value_params)//NOT VALUE PARAMS
            return next({error:`Insufficient parameters, must give value to: ${name_function}`,message:` params: ${json_func.params} -> (${code})`});
          json_func.value_params=value_params;
        }
        response = await ProxyNats.requestMaths(json_func);

      }
      else response = await ProxyNats.requestFunc(json_func);
      result = JSON.parse(response);
    } catch (error) {
      return next(error)
    }
    //3. Record the result of the operation and response to client
    if (result) {
      try {
        result.function_id = function_id;
        result.type = type;
        await CockroachDB.insertResult(function_id, result.message)
      } catch (error) {
        console.log({ error })
        return next("Error when storage the result of function")
      }
      try {
        await CockroachDB.insertResource(user_id, function_id, result.duration, '0')
      } catch (error) {
        console.log({ error })
        return next("Error inserting resource")
      }
      // save code in res(response) obj
      res.code = code;
      res.instance=result.instance;
      return res.send(result);

    } else
      return next("Error Trying to launch function ")
  });

  serverExpress.get('/currentstats', async (req, res, next) => {
    let username = req.query.username
    if (!username) return next("It's necessary to specify a username")
    //1. Check User and permission
    try {
      let queryUserID = await CockroachDB.getUser(username)
      if (queryUserID.message.length == 0) return next(`Username not registred: ${username}`);
      else if(queryUserID.message[0].user_rol!=="admin")return next(`Insufficient permissions for action`);
    } catch (error) {
      console.log(error)
      return next(`Error trying to get user from : ${username}`);
    }
    //2. Request for current stats frame
    try {
      let response = await ProxyNats.requestStatsFrame();
      return res.send(JSON.parse(response));
    } catch (error) {
      return next(error)
    }
  });

  serverExpress.get('/takedecision', async (req, res, next) => {
    const check = validate(req.query, take_decisionSchema);
    
    if (!check.valid)
      return next(check.errors.map(error => error.message));

    let decision = req.query.decision
    let instances = req.query.instances || 1
    let username = req.query.username
    let type = req.query.type
   
    if (!username) return next("It's necessary to specify a username")
    if (!decision || !type) return next("It's necessary to specify a decision and type (frontend,db,worker,nats,whatchman)")
    //1. Check User and permission
    try {
      let queryUserID = await CockroachDB.getUser(username)
      if (queryUserID.message.length == 0) return next(`Username not registred: ${username}`);
      else if(queryUserID.message[0].user_rol!=="admin")return next(`Insufficient permissions for action`);
    } catch (error) {
      console.log(error)
      return next(`Error trying to get user from : ${username}`);
    }
    //2. Request for take a decision
    try {
      let response = await ProxyNats.requestDecision({decision:decision,instances:instances,type:type});
      return res.send(JSON.parse(response));
    } catch (error) {
      return next(error)
    }
  });

  serverExpress.get('/soldecision', async (req, res, next) => {
   
    let uuid = req.query.uuid
   
    if (!uuid) return next("It's necessary to specify a uuid")
   
  // Request for sol a decision
    try {
      let response = await ProxyNats.requestSolDecision({uuid:uuid});
      return res.send(JSON.parse(response));
    } catch (error) {
      return next(error)
    }
  });
}