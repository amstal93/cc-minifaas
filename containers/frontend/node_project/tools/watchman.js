
/**
    Class that has as a main function, to obtain 
    statistics of the requests directed to the workers 
    or to the registration of new functions
 */
class WatchMan {

    constructor(ProxyNats, resolver, reject) {
        this.totalRequests = 0; // total incoming requests 
        this.ProxyNats = ProxyNats;
        if (ProxyNats) {
            if (ProxyNats.nats && ProxyNats.nats.connected)
                resolver(this)
            else reject("[WM] Watchman without acces to nats server")
        } else reject("[WM] It's necessary ProxyNats instance")
    }

    printValue() {
        console.log(this.value)
    }

    isConnectedtoNats() {
        return this.ProxyNats.nats && this.ProxyNats.nats.connected
    }

    getTotalRequest() {
        return this.totalRequests;
    }

    getDurationInMilliseconds = (start) => {
        const NS_PER_SEC = 1e9
        const NS_TO_MS = 1e6
        const diff = process.hrtime(start)
        return (diff[0] * NS_PER_SEC + diff[1]) / NS_TO_MS
    }
    // ANCHOR: STATS RESPONSE FRONTEND
    statsResponse(req, res, start) {
        let size = 0;
        let success = false;

        if (res.statusCode == 200 || res.statusCode == 204) success = true;
        const durationInMilliseconds = this.getDurationInMilliseconds(start)
        console.log(`${req.method} ${req.originalUrl} [FINISHED] ${durationInMilliseconds.toLocaleString()} ms`)
        //Only /launch operations
        if (req.originalUrl.includes("/launch")) {
            if (req.method == 'POST')
                size = ~-encodeURI(JSON.stringify(req.body)).split(/%..|./).length
            else if (req.method == 'GET') size = res.code === undefined ? req.socket.bytesRead : ~-encodeURI(JSON.stringify(res.code)).split(/%..|./).length
            this.storage_stats(req.method, req.originalUrl, durationInMilliseconds, size, success,res.instance)
        }
    }

    storage_stats(method, originalUrl, duration, size, success, instance="unknown") {
        this.totalRequests++;
        if (this.isConnectedtoNats())
            this.ProxyNats.pubStats({
                method: method,
                originalUrl: originalUrl,
                duration: duration,
                time_scale: 'ms',
                size: size,
                instance: instance,
                success: success
            })
    }
}

module.exports.create = function (ProxyNats) {
    return new Promise((resolver, reject) => {
        new WatchMan(
            ProxyNats,
            (WatchMan) => {
                console.log("[F] WatchMan.create() resolver()")
                resolver(WatchMan)
            }),
            (err) => {
                console.log("[F] WatchMan.create() reject(): " + err)
                reject(err)
            }
    })
}
/*
400 Bad Request Error:
Used when user fails to include a field (like no credit card information in a payment form)
Also used when user enters incorrect information (Example: Entering different passwords in a password field and password confirmation field).
401 Unauthorized Error: Used when user enters incorrect login information (like username, email or password).
403 Forbidden Error: Used when user is not allowed access the endpoint.
404 Not Found Error: Used when the endpoint cannot be found.
500 Internal Server Error: Used the request sent by the frontend is correct, but there was an error from the backend.
*/