CREATE USER IF NOT EXISTS root;
CREATE DATABASE IF NOT EXISTS defaultdb;
GRANT CREATE, SELECT, DROP, INSERT, DELETE, UPDATE ON DATABASE defaultdb TO root;



CREATE TABLE IF NOT EXISTS users ( 
    user_id SERIAL PRIMARY KEY, 
    username VARCHAR(100),
    UNIQUE(username),
    user_rol VARCHAR(10) DEFAULT 'user'
);
CREATE TABLE IF NOT EXISTS functions ( 
    function_id SERIAL PRIMARY KEY, 
    type VARCHAR(100), 
    function_name VARCHAR(100), 
    code VARCHAR(1000), 
    params VARCHAR(1000) NULL, 
    creator INT, 
    size INT,
    UNIQUE(function_id)
);
ALTER TABLE functions ADD FOREIGN KEY (creator) REFERENCES users(user_id);
CREATE TABLE IF NOT EXISTS results ( 
    res_id INT PRIMARY KEY DEFAULT unique_rowid(), 
    function_id INT,
    result VARCHAR(1000),
    date TIMESTAMP(3)
);
ALTER TABLE results ADD FOREIGN KEY (function_id) REFERENCES functions(function_id);
CREATE TABLE IF NOT EXISTS registers ( 
    reg_id SERIAL PRIMARY KEY, 
    user_id INT, 
    function_id INT, 
    use_time FLOAT,
    instance_type VARCHAR(100),
     date TIMESTAMP(3)
);
ALTER TABLE registers ADD FOREIGN KEY (user_id) REFERENCES users(user_id);
ALTER TABLE registers ADD FOREIGN KEY (function_id) REFERENCES functions(function_id);