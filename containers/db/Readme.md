# An example of CockroachDB and Docker Compose

Here an example of CockroachDB data base. It's based on simple 2 node insecure CockroachDB cluster.

The implementation and final structure is as follows:

1. Principal node cockroachdb that is launched via: start-single-node
2. Second node that is launched like a cluster to node 1 (--join=node_1)
3. The third container in charge of initializing the desired database and adding a user (based on [CockroachDB Remote Client](https://github.com/cockroachlabs-field/cockroachdb-remote-client))

References:

- Information on CockroachDB can be found [here](https://www.cockroachlabs.com/).
- Information on Docker Compose can be found [here](https://docs.docker.com/compose/)


Because we already have an official CockroachDB docker [image](https://hub.docker.com/r/cockroachdb/cockroach/tags), we will use that in our docker-compose.yml file. We recommend you use one of the current tags instead of latest.

#### Build and start the docker-compose with:

```bash
sudo docker-compose up --build
```
Output:

```bash
crdb-init_1  | You must supply connection parameters such as COCKROACH_HOST, COCKROACH_PORT and COCKROACH_USER
crdb-init_1  | found COCKROACH_HOST [node_1:26257]
crdb-init_1  | found COCKROACH_INSECURE [true]
crdb-init_1  | found DATABASE_NAME [test], creating...
node_1       | *
node_1       | * WARNING: ALL SECURITY CONTROLS HAVE BEEN DISABLED!
node_1       | * 
node_1       | * This mode is intended for non-production testing only.
node_1       | * 
node_1       | * In this mode:
node_1       | * - Your cluster is open to any client that can access node_1.
node_1       | * - Intruders with access to your machine or network can.. 
node_1       | * - Intruders can log in without password and read or write any data in the cluster.
node_1       | * - Intruders can consume all your server's resources and. 
node_1       | *
node_2       | *
node_2       | * WARNING: ALL SECURITY CONTROLS HAVE BEEN DISABLED!
node_2       | * 
node_2       | * This mode is intended for non-production testing only.
node_2       | * 
node_2       | * In this mode:
node_2       | * - Your cluster is open to any client that can access node_2.
node_2       | * - Intruders with access to your machine or network can .
node_2       | * - Intruders can log in without password and read or..
node_2       | * - Intruders can consume all your server's resources and
node_2       | *
node_1       | *
node_1       | * INFO: To start a secure server without mandating TLS...
node_1       | * consider --accept-sql-without-tls instead. For othersee:
node_1       | * 
node_1       | * - https://go.crdb.dev/issue-v/53404/v20.2
node_1       | * - https://www.cockroachlabs.com/docs/v20.2/secure-a-cluster.html
node_1       | *
node_2       | *
node_2       | * INFO: To start a secure server without mandating TLS....
node_2       | * consider --accept-sql-without-tls instead.For other see:
node_2       | * 
node_2       | * - https://go.crdb.dev/issue-v/53404/v20.2
node_2       | * - https://www.cockroachlabs.com/docs/v20.2/secure-a-cluster.html
node_2       | *
node_2       | CockroachDB node starting at 2021-01-01 15:45:11.122895418 
node_2       | build:               CCL v20.2.3 @ 2020/12/14 18:33:39 
node_2       | webui:               http://node_2:8080
node_2       | sql:       postgresql://root@node_2:26257?sslmode=disable
node_2       | RPC client flags:    /cockroach/cockroach <client cmd> --host=node_2:26257 --insecure
node_2       | logs:                /cockroach/cockroach-data/logs
node_2       | temp dir:/cockroach/cockroach-data/cockroach-temp722645494
node_2       | external I/O path:   /cockroach/cockroach-data/extern
node_2       | store[0]:            path=/cockroach/cockroach-data
node_2       | storage engine:      pebble
node_2       | status:              restarted pre-existing node
node_2       | clusterID:           602d3576-a90b-4111-b7c3-035def169a4e
node_2       | nodeID:              2
node_1       | CockroachDB node starting at 2021-01-01 15:45:11.118932455 
node_1       | build:          CCL v20.2.3 @ 2020/12/14 18:33:39 
node_1       | webui:               http://node_1:8080
node_1       | sql:       postgresql://root@node_1:26257?sslmode=disable
node_1       | RPC client flags:    /cockroach/cockroach <client cmd> --host=node_1:26257 --insecure
node_1       | logs:                /cockroach/cockroach-data/logs
node_1       | temp dir:/cockroach/cockroach-data/cockroach-temp551988863
node_1       | external I/O path:   /cockroach/cockroach-data/extern
node_1       | store[0]:            path=/cockroach/cockroach-data
node_1       | storage engine:      pebble
node_1       | status:              restarted pre-existing node
node_1       | clusterID:           602d3576-a90b-4111-b7c3-035def169a4e
node_1       | nodeID:              1
crdb-init_1  | CREATE DATABASE
crdb-init_1  | SET CLUSTER SETTING
crdb-init_1  | found DATABASE_USER [test] and DATABASE_PASSWORD [password], creating...
crdb-init_1  | CREATE ROLE
crdb-init_1  | GRANT
db_crdb-init_1 exited with code 0 
```

#### Start the docker-compose with:

```bash
docker-compose up
```
it will run crdb in the foreground. To run crdb in the background, pass `-d` flag to docker-compose.

```bash
docker-compose up -d
```
To view the current logs when run in the background

```bash
docker-compose logs
```

#### Launch a simple client in node:

```bash
node_project/$ node sample.js 
```

Output:

```bash
Sample Node with CockroachDB:
{ id: '620491303992066049',
  name_function: 'suma',
  name_user: 'halo',
  code_function: 'console.log(3+4)' }
{ id: '620491303992164353',
  name_function: 'resta',
  name_user: 'halo',
  code_function: 'console.log(3-4)' }
```

For more information and examples, click on the following link:

[CockroachDB and Docker examples](https://github.com/cockroachlabs-field/docker-examples)