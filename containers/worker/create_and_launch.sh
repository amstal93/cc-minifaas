#!/bin/bash
filepath=/tmp/$1.js
code=$2


if [[ -z $1 || -z $2 ]];then
	exit 127
fi

if ! location="$(type -p "nodejs")" || [ -z "nodejs" ]; then
    if ! location="$(type -p "node")"; then 
        exit 127
    fi
fi

echo $2 > $filepath

node $filepath
