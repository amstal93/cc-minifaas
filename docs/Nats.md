# Nats

Componente encargado del servicio Nats, este servicio consta de un Nats server principal con Core Nats, que es simplemente es un sistema de mensajería de disparar y olvidar, solo almacenará mensajes en la memoria y nunca los escribirá directamente en el disco.

#### Estructura

A continuación se muestra un esquema general de la estructura realizada para poder favorecer los objetivos funcionales de la implementación Faas. 

![](imgs/Nats schema.png)

#### Implementación

Para añadir mayor tolerancia a fallos, se ha implementado clúster HA en este servicio. De ese modo agrupar varios servidores juntos interconectados.

Para ello se ha realizado un script de inicialización en el Dockerfile, cuyo código es el siguiente:

```shell
#!/bin/sh
set -eu
#Little wait via sleep
sleep $WAIT
myip=$(hostname -i)
nodo=${HOSTNAME:${#HOSTNAME}-1:1}
#Get Route IPS  via port 4248
myroutes=$(nslookup $CHANNEL | awk '/Address: 1/{print}'| sed 's"Address: "Nats://"' | tail -n $INSTANCES | awk '{print}' ORS=':4248,' | sed 's/.$//')
echo "[0] CH: $CHANNEL INS: $INSTANCES IPS: $myip  IP_HOST: $myip Nodo: $nodo"
#-DV MODE DEBUG
Nats-server -p 4222 -clúster Nats://$myip:4248 -routes $myroutes -DV
#router: Routes to solicit and connect 
#clúster: Cluster URL for solicited routes
```

De este modo mediante el entorno de variable channel  se obtiene las rutas de las instancias de dicho canal, y se pasan mediante el parámetro **routes** a cada instancia de Nats que se despliegue.
