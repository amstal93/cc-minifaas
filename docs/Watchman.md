# Watchamn 

Este componentes esta escrito en Nodejs y su función principal es proporciona un sencillo monitoreo de los lanzamiento (funciones) realizadas por los usuarios.

**Tabla de contendio**
- [Watchamn](#watchamn)
    + [Implementación](#implementaci-n)
      - [Proceso de obtención de stats](#proceso-de-obtención-de-stats)
      - [Definición de cómo se van incorporando/reduciendo recursos al sistema según aumenta/disminuye la carga sobre el mismo](#definición-de-cómo-se-van-incorporando/reduciendo-recursos-al-sistema-según-aumenta/disminuye-la-carga-sobre-el-mismo)
      - [Implementación añadir o eliminar instancias](#implementación-añadir-o-eliminar-instancias)
      - [Ventajas](#ventajas)
      - [Desventajas](#desventajas)


### Implementación

Este componente se ha implementado bajo Nodejs y incluye conectividad con el servidor Nats desplegado. De esta manera se le permite accedes al trafico que se genera dentro del entorno.

#### Proceso de obtención de stats

1. Los datos son emitidos mediante el proceso **publish** de nats por las instancias de Frontend antes de enviar respuesta al cliente. 

2. Solo se emiten las respuestas generadas debido solo a las peticiones de lanzamiento.

3. A partir una respuesta se genera un paquete de datos (json) con la siguiente estructura:

```json
   {
       "method": "GET",
       "originalUrl": "/launch?name_function=suma&username=mamarbao",
       "duration": 103.171684,
       "time_scale": "ms",
       "size": 18,
       "instance": "kd-202924-1f62044f-Worker000-deployment-596cd6d968-rgb46",
       "success": true
   }
```


5. Una vez publicado un dato, es recibido y procesado por la instancia Watchman.

6. Se verifica mediante **jsonschema**, que los datos recibidos siguiente el formato correcto.

7. Los **datos** (stats) son **almacenados en una cola**, con un limite de elementos (10).

Se ha elegido una cola con limite de elemento, para tener sola la información de las ultimas operaciones de los clientes. Cuando se sobrepasa el limite, se desencola el primer elemento y se encola el uno nuevo.



#### Definición de cómo se van incorporando/reduciendo recursos al sistema según aumenta/disminuye la carga sobre el mismo

1. Cada cierto tiempo, el componente Watchman analiza la cola para tomar una decisión al respecto, en este caso siguiendo la siguiente lógica:
   1. Si la media de tiempo de respuesta supera un limite de umbral, un nuevo Worker es instanciado
   2. Si la media de respuesta no llega un limite de umbral inferior, un Worker es eliminado.

Con ello se consigue un simple agente cuya función es controlar las cargas de trabajo, y tomar una decisión cada cierto tiempo. 



#### Implementación añadir o eliminar instancias

Para poder llevar acabo este proceso , ha sido utilizada la herramienta Kumori `kumorictl` con ayuda de dos shell scripts. Estos scripts son lanzados por el Watchman en Nodejs. A continuación los pasos para modificar las instancias de los Workers (es posible manejar instancias de otros componentes)

1. Primero
   - Modifica el numero de instancias de Worker del desployment (en mainfest.cue) con las siguientes restricciones:
     - Aumenta el numero de Workers, si actualmente el número de Workers es menor a 6
     - Decremento del número de Workers, siempre que el número de Workers sea mínimo 1.
2. Segundo
   - Realiza automáticamente el login con *kumorictl login* y realiza el *update*.



#### Ventajas

- Se puede controlar el número de instancias dependiendo del trafico.

- Se puede mejorar los scripts para hacer uso de `kumorictl` y visualizar el estado de las instancias y en su defecto, realizar una decisión adecuada o precisa.

- Es posible implementar que se modifiquen otras instancias (ahora acotado a Workers)

- Existe la posibilidad de modificar la versión del contenedor especificado en un componente y realizar el *update*.


#### Desventajas

- Es necesario que el contenedor tenga todos los ficheros del deployment

- Se tiene que conocer exactamente como se llama el componente y tener cuidado con las instancias desplegadas.