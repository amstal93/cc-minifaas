# Quick Start

Necesario tener descargado

- The `kumorictl` tool can be downloaded from [its releases page](https://gitlab.com/kumori-systems/community/tools/kumorictl/-/releases)

- 0.2.2' of CUE ([cue_0.2.2_Linux_arm64.tar.gz](https://github.com/cuelang/cue/releases/download/v0.2.2/cue_0.2.2_Linux_arm64.tar.gz))


Ahora tendremos 2 carpetas (descomprimir):

- kumorictl-v0.3.2

- cue_0.2.2_Linux_x86_64


### Preparar entorno

Dentro de **cue_*/**:

Compilamos con Go :

```
go build
```

Se crea el ejecutable **cue**, lo copiamos en **/usr/local/bin** para **utilizarlo como un comando**



Dentro de **kumorictl-v*/**:

Compilamos con Go :

```
go build
```

Se crea el ejecutable **kumorictl**, lo copiamos en **/usr/local/bin** para **utilizarlo como un comando**

Al final en **/usr/local/bin**:

```
-rwxr-xr-x  1 root root 15474688 nov  4 16:27 cue*
-rwxr-xr-x  1 root root 19536033 nov  4 16:40 kumorictl*
```



### Iniciar KumoriCLI y Login:

Vamos a una carpeta donde queremos iniciar un nuevo proyecto y realizamos los siguientes pasos (usuario ejemplo **mamarbao**):

```shell
/project-kumori$ kumorictl init
WARN	Workspace not yet initialized. Using defaults.
Workspace initialized. You may want to use kumorictl config to customize configuration parameters.
```

```shell
/project-kumori$ 
$ kumorictl config --admission admission-ccmaster.vera.kumori.cloud
Workspace configuration updated.
```

```
/project-kumori$ kumorictl login mamarbao

Enter Password:  <<DNI sin Letra>>
Login OK.
```

#### Certificados y dominio

Dentro de la carpeta actual , se ha creado un subdirectorio ./certs con el siguiente contenido:

```shell
-rw-rw-r-- 1 manu manu 3570 oct 28 11:56 wildcard.vera.kumori.cloud.crt
-rw-rw-r-- 1 manu manu 1704 oct 28 11:56 wildcard.vera.kumori.cloud.key
```

Set a default domain

```shell
kumorictl config --user-domain mamarbao.examples
```

#### Register a (wildcard) certificate

Note: nombre del certificado terminado en **.wildcard**

```shell
$ kumorictl register certificate calccert.wildcard \
  --domain *.vera.kumori.cloud \
  --cert-file ./certs/wildcard.vera.kumori.cloud.crt \
  --key-file ./certs/wildcard.vera.kumori.cloud.key
```
#### **Register the inbound** 

Nombrado **calcinb**

- **--domanin:** change my_user to mamarbao (example: mamarbao, username de este ejemplo)
- **--cert:** use the reference obtained via `kumorictl get all`

```
Certificates:
mamarbao.examples/calccert.wildcard
```



```shell
$ kumorictl register http-inbound calcinb \
  --domain calculator-mamarbao.vera.kumori.cloud \
  --cert mamarbao.examples/calccert.wildcard \
  --comment "My first inbound"
```
#### Register the deployment, without cache

Nombrado **calcdep**

```shell
$ kumorictl register deployment calcdep \
  --deployment ./cue-manifests/deployment_nocache \
  --comment "My deploy without cache"
  ##Salta error de dependencias, realizar FETCH de las dependencias de la carpeta cua-manifests
```

```shell
$ kumorictl fetch-dependencies cue-manifests/.
```

Note: la carpeta **cue-manifest**/ se obtiene de la descarga del este repositorio:

https://gitlab.com/kumori-systems/community/examples/calc-cache/-/tree/master/

```shell
$ kumorictl register deployment calcdep   --deployment ./cue-manifests/deployment_nocache   --comment "My deploy without cache"
  adding: manifests/ (stored 0%)
  adding: manifests/Manifest.json (deflated 86%)
Deployment mamarbao.examples/calcdep created
```

**See the elements in the platform**

```shell
$ kumorictl get all
Deployments:
mamarbao.examples/calcdep
Http-Inbounds:
mamarbao.examples/calcinb
Certificates:
mamarbao.examples/calccert.wildcard
```
#### Link inbound and deployment:

```shell
$ kumorictl link calcdep:service calcinb
```
#### Describe the deployment
```shell
$ kumorictl describe deployment calcdep

Deployment : mamarbao.examples/calcdep
Owner : mamarbao@inf.upv.es
Comment : My deploy without cache
Service : kumori.systems.examples/service/calccache/0_0_1

State :
		[Tabla de componenetes]
Links :
  mamarbao.examples/calcdep:service <=> mamarbao.examples/calcinb

```

#### Describe inbound

```shell
project-kumori$ kumorictl describe http-inbound mamarbao.examples/calcinb
Http-Inbound : mamarbao.examples/calcinb
Owner : mamarbao@inf.upv.es
Comment : My first inbound
Domain : calculator-myuser.vera.kumori.cloud
TLS : true
ClientCert : false
Websocket : false
Certificate : mamarbao.examples/calccert.wildcard
Links :
  mamarbao.examples/calcinb <=> mamarbao.examples/calcdep:service
```
#### Test
```shell
project-kumori$ curl https://calculator-mamarbao.vera.kumori.cloud/getenv
CALCULATOR_ENV = The_calculator_env_valuem

project-kumori$ curl https://calculator-mamarbao.vera.kumori.cloud/file/config.json
{
  "param_one": "myparam_one",
  "param_two": 123
}

project-kumori$ curl -d '{"expr":"1+2+3"}' -H "Content-Type: text/plain" -H "usecache: true" -X POST https://calculator-mamarbao.vera.kumori.cloud/calc
{"expr":"1+2+3","res":"6.00","frontendID":"kd-163919-3c512969-frontend000-deployment-67c4cf5bd8-8cbx6","workerID":"kd-163919-3c512969-worker000-deployment-1"}
```



### Reset

En en siguiente orden:

1. quitar link del inbound (**inbound:**calcinb **deployment**:calcdep)
2. quitar el deployment
3. quitar el inboud

```shell
 $ kumorictl unlink calcdep:service calcinb
 $ kumorictl unregister deployment  calcdep
 $ kumorictl unregister http-inbound calcinb
```

##### Eliminar certificados

```shell
$ kumorictl get all
Deployments:
mamarbao.examples/calcdepfinal
Http-Inbounds:
mamarbao.examples/calcinbfinal
Certificates:
mamarbao.examples/calccert.wildcard
mamarbao.examples/calccert2.wildcard
```

```shell
$ kumoritcl unregister certificate mamarbao.examples/calccert2.wildcard
```

##### Volver a re-hacer correctamente:

EJemplo con:

- **deployment:** calcinbfinal
- **inbound:** calcdepfinal

```shell
$ kumorictl register http-inbound calcinbfinal   --domain calculator-mamarbao.vera.kumori.cloud   --cert mamarbao.examples/calccert.wildcard   --comment "My first inbound"

$ kumorictl register deployment calcdepfinal   --deployment ./cue-manifests/deployment_nocache   --comment "My deploy without cache"

$ kumorictl link calcdepfinal:service calcinbfinal
```

Ver estado nuevo:

```shell
#========DEPLOYMENT========

$ kumorictl describe deployment calcdepfinal
Deployment : mamarbao.examples/calcdepfinal
Owner : mamarbao@inf.upv.es
Comment : My deploy without cache
Service : kumori.systems.examples/service/calccache/0_0_1

State :
..................................................................
..................................................................

Links :
  mamarbao.examples/calcdepfinal:service <=> mamarbao.examples/calcinbfinal

#========INBOUND========

$ kumorictl describe http-inbound mamarbao.examples/calcinbfinal
Http-Inbound : mamarbao.examples/calcinbfinal
Owner : mamarbao@inf.upv.es
Comment : My first inbound
Domain : calculator-mamarbao.vera.kumori.cloud
TLS : true
ClientCert : false
Websocket : false
Certificate : mamarbao.examples/calccert.wildcard
Links :
  mamarbao.examples/calcinbfinal <=> mamarbao.examples/calcdepfinal:service
```



### Posibles problemas

##### Error get ENV

```shell
Error get ENV curl https://calculator-mamarbao.vera.kumori.cloud/getenv
curl: (6) Could not resolve host: calculator-mamarbao.vera.kumori.cloud
```

Es posible que al realizar el registro de inbound (`kumorictl register http-inbound <name>`) no se haya especificado tal dominio (`calculator-mamarbao.vera.kumori.cloud`)

Por ejemplo, aquí esta el error:

```shell
# Se ha hecho referencia al dominio calculator-myuser, ya esta siendo utilizado
$ nslookup calculator-myuser.vera.kumori.cloud
Server:		127.0.0.53
Address:	127.0.0.53#53

Non-authoritative answer:
calculator-myuser.vera.kumori.cloud	canonical name = ingress-ccmaster.vera.kumori.cloud.
Name:	ingress-ccmaster.vera.kumori.cloud
Address: 91.235.109.234

$ nslookup calculator-mamarbao.vera.kumori.cloud
Server:		127.0.0.53
Address:	127.0.0.53#53

** server can't find calculator-mamarbao.vera.kumori.cloud: NXDOMAIN
```

Solución eliminar todo como en el apartado **Reset** y realizarlo nueva mente, asegurarse de registrar el **inboud** correctamente con nombre de dominio:

Para los ejemplos utilizados en este documento:

```shell
 --domain calculator-mamarbao.vera.kumori.cloud
```

##### Fallo con SSL

```shell
project-kumori$ curl https://calculator-mamarbao.vera.kumori.cloud/getenv
curl: (35) OpenSSL SSL_connect: SSL_ERROR_SYSCALL in connection to calculator-mamarbao.vera.kumori.cloud:443 
```

Posibles causas:

- Tener mas de un certificado asignado
- Mal referenciado el certificado al registrar el **inboud**

###### Solución:

Mirar si tenemos mas certificado, en ese caso :

```shell
$ kumorictl get all
......
Certificates:
mamarbao.examples/calccert.wildcard
mamarbao.examples/calccert2.wildcard
```

```shell
$ kumoritcl unregister certificate <referencia del certificado>
```

Mirar que la referencia del certificado del **inboud**, esto esta mal:

```shell
$ kumorictl describe http-inbound mamarbao.examples/calcinbfinal
Http-Inbound : mamarbao.examples/calcinbfinal
Owner : mamarbao@inf.upv.es
Comment : My first inbound
Domain : calculator-mamarbao.vera.kumori.cloud
TLS : true
ClientCert : false
Websocket : false
Certificate : calccert.wildcard
Links :
  mamarbao.examples/calcinbfinal <=> mamarbao.examples/calcdepfinal:service
```

La referencia **calccert.wildcard** no es correcta:

```shell
$ kumorictl get all
Deployments:
mamarbao.examples/calcdepfinal
Http-Inbounds:
mamarbao.examples/calcinbfinal
Certificates:
mamarbao.examples/calccert.wildcard
```

Eliminar el **inboud** mediante los 3 pasos del apartado **Reset** del documento y asignar el certificado correcto:

```shell
-cert mamarbao.examples/calccert.wildcard
```



