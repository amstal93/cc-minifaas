# Posibles mejoras

Debido al tiempo limitado de desarrollo del trabajo, han quedado pendiente algunas posibles mejoras o apelaciones que se van a exponer a continuación.

## Funciones

- Poder realizar una especie de middleware que compruebe el código facilitado por el cliente
- Esquematizar o estandarizar las funciones con mas finura .

## Lógica Watchman

Este componente es el encargado de tomar decisiones dependiendo del rendimiento general del Faas desplegado. Actualmente se ha podido lograr eliminar un Worker cuando la carga del trabajo es insuficiente o instanciarlo.

- Por ahora es el Frontend quien proporciona al usuario con rol admin esta posibilidad, sería conveniente realizar otro Frontend específicamente para ofrecer operaciones de administración.
- Al lograr poder interaccionar con el cliente `kumorictl`, ahora es posible implementar mas funcionalidades en este componente como:
  - Mostrar el estado actual de todos o algún componente.
  - Llevar un mayor control ante la saturación de algún componente en el sistema, tomando las medidas que sean oportunas.
  - Poder actualizar la versión de un componente sin tener que volver a desplegar todo de nuevo.
- Poder implementar un lógica de toma de decisiones mas perfeccionada ante los datos obtenidos mediante las peticiones de los clientes, con el debido tiempo se pueden realizar pruebas de rendimiento o congestión del despliegue con herramientas ya existentes.

## Base de datos

Ajustes en los nodos desplegados

- **Sincronización de Relojes**

  CockroachDB para preservar la coherencia de los datos necesita tener una sincronización de relojes.

  Cuando un nodo detecta que su reloj no está sincronizado con demás nodos (mitad de ellos mínimo) del clúster en un 80% del máximo permitido (500 ms predeterminada), se apaga espontáneamente. Esto evita el riesgo de anomalías de coherencia, es importante que los relojes no se desvíen demasiado.

  Esto se puede prevenir ejecutando un software de sincronización de reloj en cada nodo.

- **Configuración balanceador de cargar**

  La implementación actual del clúster HA solo añade tolerancia a fallos y replicación, pero no reparte las cargas entre los distintos nodos instanciados. 

  Cada nodo de CockroachDB es una puerta de enlace SQL (*clúster*), pero para garantizar el rendimiento y la confiabilidad del cliente, es importante utilizar un balanceador de carga. Para poder ofrecer:
  - Mejora el rendimiento: el tráfico se distribuye entre los nodos. Esto evita que un nodo se vea abrumado por solicitudes y mejora el rendimiento general del clúster.
  - Mejora la fiabilidad: En los casos en que falla un nodo, el balanceador de carga redirige el tráfico del cliente a los nodos disponibles.


Una implementación futura, sería utilizar [HAProxy](http://www.haproxy.org/) un balanceador de carga TCP de código abierto, y CockroachDB incluye un comando integrado para generar un archivo de configuración para funcionar con los clústers desplegados.

  **Nota:**

  Con **un solo balanceador** de carga, las conexiones de los clientes son **resistentes a las fallas del nodo**, pero **el balanceador de carga en sí es un punto de fallo**. Esto podemos solventarlo añadiendo varias instancias *load balancer*, y utilizando una IP flotantes o DNS para seleccionar el balanceador de carga para los clientes.

  [Referencia deploy cockroachdb](https://www.cockroachlabs.com/docs/v20.2/deploy-cockroachdb-on-premises-insecure#requirements)

  
