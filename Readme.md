# MiniFaas

**Tabla de contenido**

- [MiniFaas](#minifaas)
  * [Contexto](#contexto)
    + [Tecnologías utilizadas](#tecnologías-utilizadas)
  * [Despligue](#despligue)
    + [Despliegue kumori](#despliegue-kumori)
  * [Funcionamiento y Componentes](#funcionamiento-y-componentes)
    + [Frontend](#frontend)
    + [Worker](#worker)
    + [Nats](#nats)
    + [CockroachDB](#cockroachdb)
    + [Watchman](#watchman)
  * [Especificación de una función](#especificación-de-una-funci-n)
  * [Uso](#uso)
    + [Requests API](#requests-api)
    + [Lanzamiento](#lanzamiento)
      - [Ejemplos de funcionalidad](#ejemplos-de-funcionalidad)
      - [Esquema de interacción lanzamiento](#esquema-de-interacción-lanzamiento)
  * [Futuras mejoras](#futuras-mejoras)



## Contexto

En este proyecto se ha desarrollado un sistema que permite la ejecución dinámica de funciones por parte de usuarios, haciendo uso de la plataforma PaaS Kumori.

### Tecnologías utilizadas

La tecnología principal en la implementación del proyecto es el lenguaje de programación [NodeJS](https://nodejs.org/es/), con el que se ha creado el frontend una API REST para gestionar las diferentes funcionalidades del sistema, Workers cuya función es ejecutar las funciones, Watchman que actúa de vigilante del sistema desplegado.

* [Dockerfile](https://docs.docker.com/engine/reference/builder/) : Mediante los ficheros Dockerfile se han configurado los contenedores necesarios para el despliegue del sistema.
* [NATS](https://nats.io/): Internamente la API hace uso del sistema de colas NATS para la comunicación entre el frontend y los nodos encargados de la ejecución de las funciones.
* [CockroachDB](https://www.cockroachlabs.com/) : Se ha utilizado como base de datos para los diferentes datos que maneja el sistema.
* [Kumori](https://kumori.systems/) : La aplicación se ha alojado en esta plataforma PaaS.

Por otra parte se han implementado una serie de *shell scripts* para el lanzamiento de funciones en *nodejs* o *javascript* y facilitar la configuración de la inicialización de contenedores como NATS o CockroachDB.



## Despligue



### Despliegue kumori

![](docs/imgs/MiniFaas%20KUmori%20Diagram.png)

## Funcionamiento y Componentes

El funcionamiento del sistema desplegado Faas trata sobre la interacción de los diferentes componentes para lograr mediante una petición HTTP de un cliente, realizar el lanzamiento de una función registrada por parte de una serie de *Workers* desplegados. Siendo posible la comunicación mediante colas nats y la persistencia con CockroachDB. Siendo monitorizada esta operación por el componente *Watchman*.

### Frontend

El frontend es el encargado de proporcionar interacción con el cliente y ofrecer las funcionalidades y servicios del sistema Faas implementado. En rasgo generales, consta de un servidor **HTTP REST** que hace uso de una serie de librerías para comunicarse bajo **NATS** y manejar la persistencia de datos mediante el uso de una base de datos **CockroachDB.**

Se ha implementado bajo el lenguaje nodejs juntos a una serie de paquetes esenciales:

- `express`
- `nats`
- `pg`
- `jsonschema`

Algunas de las funcionalidades de este componente son:

- **Recibir peticiones HTTP**

  La interacción con el cliente o usuario se lleva acabo mediante peticiones HTTP GET y POST.

  En el documento [Requests.md](./docs/Requests.md) se detalla cada una de las posibles peticiones disponibles y la salida o salidas que se generan.

- **Conexión a la base de datos**

  Realiza una conexión de tipo *Pool* con la base de datos para así mantener una única conexión por instancia del **Frontend**. Al iniciar el Frontend se inicializa la base de datos y se generan los siguientes datos:

  | Dato  | Información |
  |-----------------|:--------------|
  |Usuario: *mamarbao*|Tipo invitado|
  |Usuario: *manager*|Tipo administrador|
  |Función: *suma*|Tipo JS asignada al usuario *mamarbao*|
  |Función: *resta*|Tipo maths asignada al usuario *mamarbao*|
  | Funcion: *areaTriangulo* | Tipo maths con parametros asignada al usuario *mamarbao* |

    > Nota: Estos datos son creados solo una vez y únicamente si no existen.

- **Conexión con NATS**

  Se realizan una serie de requests para mantener la comunicación con los otros componentes desplegados con la siguiente configuración
    1. En la request solo se acepta una respuesta. Después es eliminada.
    2. Contiene un timeout en caso de que la respuesta por parte de un worker exceda un tiempo especificado.
  

Para más información sobre el componerte Frontend acceder al siguiente documento: [Frontend](./docs/Frontend.md)

### Worker

Encargado de ejecutar las funciones registradas por el sistema que son enviadas mediante mensajes en formato json vía Nats. En el siguiente documento se entra más en detalle: [Worker](./docs/Worker.md)

**La especificación de las funciones** esta en el apartado [Especificación de una función](#Especificación de una función)

### Nats

Este componente es el encargado de interconectar los componentes como **frontend**, **workers** y el **watchman**. Además ha sido integrado en modo clúster HA. 

En el siguiente documento se profundiza más sobre este componente y su configuración: [NATS](./docs/Nats.md)

### CockroachDB

La base de datos contiene información de:

-	Usuarios: Se almacena información de los usuarios para llevar un seguimiento.
-	Funciones: Se almacenan las diferentes funciones y sus códigos, además de otra información como el usuario que ha registrado la función.
-	Resultados: Se almacenan todos los resultados de cada ejecución.
-	Recursos: Se registra toda la información relativa al consumo de recursos de cada cliente; las funciones que ejecuta y el tiempo de ejecución.

Se crea una imagen docker de la base de datos, configurado de forma automática mediante un script en bash para permitir una configuración modo clúster HA. 

Para más detalle de la implementación modo clúster y estructura de tablas: [CockroachDB.md](./docs/CockroachDB.md)

### Watchman

Actúa como un componente de motorización del estado del despliegue,  y proporciona algunas funcionalidad para su administración. Actuando con una lógica definida para considerar cuando es necesario crear o eliminar más instancias de workers.

Todo ello se detalla en el siguiente documento: [Watchman.md](./docs/Watchman.md)



## Especificación de una función

Existen dos tipos de funciones:

1. Maths
2. javascript/nodejs


**1. Funciones javascript/node**

Son fragmentos de código en Node o javascript, que se ejecutan mediante un fichero shell script que recibe el código y lo ejecuta devolviendo el resultado.

**Restricciones:**

El código suministrado, debe terminar con un printeo del resultado (*e.x console.log*) del resultado que se desea devolver, por ejemplo:

```json
{
    "type": "javascript",
    "function_name": "fibonacci",
    "creator": "626169711802744833",
	"code": "function fibonacci(num){var a = 1, b = 0, temp;while (num >= 0){temp = a;a = a + b;b = temp;num--;}return b;};console.log(fibonacci(24))"
}
```

> Nota: El campo creator puede ser omitido, se puede registrar la función indicando por parámetros el nombre de usuario (username)

**2. Funciones Maths**

Este tipo de funciones, se basan en realizar operaciones matemáticas, con posibilidad de crear funciones con valores fijos o paso por parámetros.

El registro se realiza mediante un HTTP Post pasando el siguiente json:

**Sin restricciones:**

Estas no utilizan ningún fichero shell script, son ejecutadas directamente por el worker utilizando la librería maths ([mathjs documentation](https://mathjs.org/docs/index.html)).

- Función tipo maths con parámetros (*areaTrapecio*):

  - ```json
    {
        "type": "maths",
        "function_name": "areaTrapecio",
        "creator": "631844972575227905",
        "code": "((b+a)/2)*h",
        "params": "a,b,h"
    }
    ```

- Función tipo maths sin parámetros (*areaTrapecio-notParams*):

  - ```
    {
        "type": "maths",
        "function_name": "areaTrapecio-notParams",
        "creator": "631844972575227905",
        "code": "((12+15)/2)*6"
    }
    ```

  
  > Nota: El campo creator puede ser omitido, se puede registrar la función indicando por parámetros el nombre de usuario (username)

**Importante sobre la Definición de las funciones**

El **frontend** realiza una comprobación de que tanto las funciones de tipo maths como javascript contienen todos los campos necesarios. Se realiza con una validación contra un  esquema json ([jsonschema](https://www.npmjs.com/package/jsonschema)) definido. Aquí el esquema definido: [juncSchema.json](./containers/frontend/node_project/routes/schema/funcSchema.json)

Ademas el código matemático con funciones, cuando se va ha registrar se comprueba:

1. El número y nombre de los parámetros coinciden con el código declarado
2. Los parámetros están bien especificados (ex "a,b,c...")
3. En caso de variables repetidas, las ignora

En el documento [Request](./docs/Requests.md) en la sección de **Funciones** están descritas las peticiones y sus correspondientes salidas para:

- Registro de una función 
- Registro de una función via username
- Registro de una función con parámetros de tipo maths
- Envió de una petición de ejecución o Lanzamiento de una función
- Recepción del resultado de una ejecución

**Nota:** No se ha podido implementar con parámetros el tipo de función **Javascript** por falta de tiempo.

## Uso

Para comprobar el funcionamiento del Faas desplegado, se requiere utilizar una serie de peticiones HTTP. 

### Requests API

Todas las peticiones que dispone este Faas están expuestas en detalle en el siguiente documento: [Requests](./docs/Requests.md)

### Lanzamiento

#### Ejemplos de funcionalidad

Para poder facilitar el uso del Faas desplegado, se ha compartido una plantilla creada con **Postman** ordenada ([plantilla Minifaas](./docs/postman/MiniFaas Kumori Platform.postman_collection)), de forma que se empieza registrando un usuario y una función, se realiza el lanzamiento de una función y se termina realizado una consulta de las estadísticas actuales sobre los lanzamientos (estado actual).

El orden más detallado sería el siguiente:

- **0** Registry User
- **1** Registy Function without params
- **1**.**b** Registy Function with params
- **1.c** Registy Simple Function
- **1.d** Registy Function via Username
- Optional B-Get All Users
- Optional A-Get All Functions
- Optional C-GET All sol Function
- **2** Launch Function
- **2.b** Launch Function type math with params
- **2.c** 2.c Launch func type maths without params
- **3** Get User Resouces
- **4** Get Current Stats of Faas
- **5** Take Decision (Instance o delete instance of component)
- **6** Result Decision

Proceso de importación: [Importing and exporting data](https://learning.postman.com/docs/getting-started/importing-and-exporting-data/)

#### Esquema de interacción lanzamiento


Para poder realizar el lanzamiento de una función, es necesario tener registrada la función y suministrar la id del usuario que ha registrado la función.

En este esquema se muestra a grandes rasgos los diferentes componentes implicados y sus interacciones más importantes para de tratar una petición de lanzamiento de una función ya registrada:

![](docs/imgs/FrontendLanzamineto_peticion.png)

Como se puede observar los pasos son los siguientes:

- **Step 1:** Empieza solicitando la función "suma" 
- **Step 2:** Se busca el código de la función en la BD
- **Step 3:** Es emitido un mensaje con la función por nats
- **Step 4:** Un worker disponible genera un respuesta a la resolución de la función
- **Step 5:** Se almacena el resultado junto a los recursos utilizados en la BD
- **Step 6:** Las estadísticas generadas por la petición se envía al Watchman por nats y se envía la respuesta al cliente (HTTP response). Estas estadísticas (stats) pueden ser consultadas por el usuario con rol de administrador.



## Futuras mejoras

Debido al tiempo limitado no se ha podido perfeccionar el despliegue Faas implementado, en el siguiente documento algunas futuras mejoras que podrían ser realizadas:  [Mejoras](./docs/Mejoras.md)